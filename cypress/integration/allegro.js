describe('Rekrutacja Allegro - testy akceptacyjne', () => {
    context('Get IDs of Allegro categories', () => {
        it('GET /sale/categories with parentId', () => {
            cy.getToken().then((token) => {
                cy.request({
                    url:
                        'https://api.allegro.pl/sale/categories?parent.id=954b95b6-43cf-4104-8354-dea4d9b10ddf',
                    headers: {
                        Authorization: token,
                        Accept: 'application/vnd.allegro.public.v1+json',
                    },
                }).then((allegroCategories) => {
                    // Oczekuj listy kategorii i statusu 200 w odpowiedzi
                    expect(allegroCategories.status).to.equal(200);
                    expect(allegroCategories.body.categories).to.be.an('array');
                });
            });
        });

        it('GET /sale/categories without parentId', () => {
            cy.getToken().then((token) => {
                cy.request({
                    url: 'https://api.allegro.pl/sale/categories',
                    headers: {
                        Authorization: token,
                        Accept: 'application/vnd.allegro.public.v1+json',
                    },
                }).then((allegroCategories) => {
                    // Oczekuj listy kategorii i statusu 200 w odpowiedzi
                    expect(allegroCategories.status).to.equal(200);
                    expect(allegroCategories.body.categories).to.be.an('array');
                });
            });
        });

        it('GET /sale/categories with wrong parentId', () => {
            cy.getToken().then((token) => {
                cy.request({
                    url:
                        'https://api.allegro.pl/sale/categories?parent.id=wrongCategoryId',
                    headers: {
                        Authorization: token,
                        Accept: 'application/vnd.allegro.public.v1+json',
                    },
                    failOnStatusCode: false,
                }).then((allegroCategories) => {
                    // Oczekuj statusu 404 w odpowiedzi
                    expect(allegroCategories.status).to.equal(404);
                });
            });
        });
    });

    context('Get a category by ID', () => {
        it('GET /sale/categories/{categoryId} with valid categoryId', () => {
            var categoryId = '6061';
            cy.getToken().then((token) => {
                cy.request({
                    url: 'https://api.allegro.pl/sale/categories/' + categoryId,
                    headers: {
                        Authorization: token,
                        Accept: 'application/vnd.allegro.public.v1+json',
                    },
                }).then((allegroCategory) => {
                    // Oczekuj szczegółów kategorii i statusu 200 w odpowiedzi
                    expect(allegroCategory.status).to.equal(200);
                    expect(allegroCategory.body.id).to.equal(categoryId);
                    expect(allegroCategory.body.name).to.equal('Doom Trooper');
                });
            });
        });

        it('GET /sale/categories/{categoryId} with invalid categoryId', () => {
            var invalidCategoryId = '1a2b3c4d5e6f';
            cy.getToken().then((token) => {
                cy.request({
                    url:
                        'https://api.allegro.pl/sale/categories/' +
                        invalidCategoryId,
                    headers: {
                        Authorization: token,
                        Accept: 'application/vnd.allegro.public.v1+json',
                    },
                    failOnStatusCode: false,
                }).then((allegroCategory) => {
                    // Oczekuj statusu 404 w odpowiedzi
                    expect(allegroCategory.status).to.equal(404);
                });
            });
        });
    });

    context('Get parameters supported by a category', () => {
        it('GET /sale/categories/{categoryId}/parameters with valid categoryId', () => {
            var categoryId = 709;
            cy.getToken().then((token) => {
                cy.request({
                    url:
                        'https://api.allegro.pl/sale/categories/' +
                        categoryId +
                        '/parameters',
                    headers: {
                        Authorization: token,
                        Accept: 'application/vnd.allegro.public.v1+json',
                    },
                }).then((allegroCategoryParameters) => {
                    // Oczekuj listy parametrów i statusu 200 w odpowiedzi
                    expect(allegroCategoryParameters.status).to.equal(200);
                    expect(allegroCategoryParameters.body.parameters).to.be.an(
                        'array'
                    );
                });
            });
        });

        it('GET /sale/categories/{categoryId}/parameters with invalid categoryId', () => {
            var invalidCategoryId = 123456789987654321;
            cy.getToken().then((token) => {
                cy.request({
                    url:
                        'https://api.allegro.pl/sale/categories/' +
                        invalidCategoryId +
                        '/parameters',
                    headers: {
                        Authorization: token,
                        Accept: 'application/vnd.allegro.public.v1+json',
                    },
                    failOnStatusCode: false,
                }).then((allegroCategoryParameters) => {
                    // Oczekuj statusu 404 w odpowiedzi
                    expect(allegroCategoryParameters.status).to.equal(404);
                });
            });
        });
    });
});
