Cypress.Commands.add('getToken', () => {
    const authorizeUrl = 'https://allegro.pl/auth/oauth/token';
    const clientId = 'cb3d9ebe6aa24f2986ce3f6a8024413b';
    const clientSecret =
        'PcgiT9hDF43s0CVMtjTmMoqT5wvHIBXxUxjDSBvjwX5KsTbE2StEyZHf1lHhfcR0';
    const credentials = btoa(clientId + ':' + clientSecret);
    const loginRequest = {
        method: 'POST',
        url: `${authorizeUrl}`,
        qs: {
            grant_type: 'client_credentials',
        },
        headers: {
            Authorization: 'Basic ' + credentials,
        },
    };
    cy.request(loginRequest).then((loginRequest) => {
        return 'Bearer ' + loginRequest.body.access_token;
    });
});
