Przypadki testowe znajdują się w folderze `cypress/integration`. Funkcje pomocnicze (wyciąganie tokena) znajdują się w pliku `cypress/support.commands.js`.

Aby odpalić projekt należy pobrać repozytorium i wykonać polecenie `npm install`. Testy można odpalić na dwa sposoby:

1. W trybie headless za pomocą polecenia `npx cypress run`.
2. W przeglądarce za pomocą polecenia `npx cypress open` a następnie wybierając plik z dashboardu.

Więcej: https://docs.cypress.io/guides/getting-started/installing-cypress.html#Opening-Cypress

